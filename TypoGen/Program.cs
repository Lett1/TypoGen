﻿using System;
using System.Collections.Generic;

namespace TypoGen
{
    internal class Program
    {

        public static void Main(string[] args)
        {
            string teststring = @"The quick brown Fox jumps over the lazy Dog 1234567890 -+ [] \ ; ' , . /";
            string doublelettertest = @"cheese, bottle, freely, vessel, sheets, sudden, copper, fallen, differ, gotten";
            string suffixtest =
                @"binary battery bribery letter mower mover easy fantasy completely accident argument";

            var typoGen = new TypoGenerator();

            Console.WriteLine("Transposing Letters");

            for (int i = 0; i < 10; i++)
            {
                foreach (var word in teststring.Split(' '))
                {
                    Console.Write(typoGen.TransposeLetters(word));
                    Console.Write(" ");
                }
                Console.WriteLine();
            }

            Console.WriteLine("Mistyping keys");

            for (int i = 0; i < 10; i++)
            {
                foreach (var word in teststring.Split(' '))
                {
                    Console.Write(typoGen.MistypeKeys(word, i*10));
                    Console.Write(" ");
                }
                Console.WriteLine();
            }


            Console.WriteLine("Collapsing double Letters");

            for (int i = 0; i < 10; i++)
            {
                foreach (var word in doublelettertest.Split(' '))
                {
                    Console.Write(typoGen.CollapseDoubleLetters(word, i*10));
                    Console.Write(" ");
                }
                Console.WriteLine();
            }

            Console.WriteLine("Sticky shift key");

            for (int i = 0; i < 10; i++)
            {
                foreach (var word in teststring.Split(' '))
                {
                    Console.Write(typoGen.ShiftLeak(word, i*10));
                    Console.Write(" ");
                }
                Console.WriteLine();
            }

            Console.WriteLine("Doubling letters");

            for (int i = 0; i < 10; i++)
            {
                Console.Write("{0}% ", i*10);
                foreach (var word in teststring.Split(' '))
                {
                    Console.Write(typoGen.DoubleLetters(word, i*10));
                    Console.Write(" ");
                }
                Console.WriteLine();
            }

            Console.WriteLine("Suffix errors");

            for (int i = 0; i < 11; i++)
            {
                Console.Write("{0}% ", i*10);
                foreach (var word in suffixtest.ToUpper().Split(' '))
                {
                    Console.Write(typoGen.SuffixTypo(word, i*10));
                    Console.Write(" ");
                }
                Console.WriteLine();
            }

            Console.WriteLine("Similar sounding letters");

            for (int i = 0; i < 11; i++)
            {
                Console.Write("{0}% ", i*10);
                foreach (var word in teststring.Split(' '))
                {
                    Console.Write(typoGen.SimilarSoundTypo(word, i*10));
                    Console.Write(" ");
                }
                Console.WriteLine();
            }

            Console.WriteLine("Random capitalisation of words");

            for (int i = 0; i < 11; i++)
            {
                Console.Write("{0}% ", i*10);
                foreach (var word in teststring.Split(' '))
                {
                    Console.Write(typoGen.RandomCapsWord(word, i*10));
                    Console.Write(" ");
                }
                Console.WriteLine();
            }
        }
    }

}