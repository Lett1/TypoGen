﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace TypoGen
{
    public class TypoGenerator
    {
        private readonly Random _random;

        public TypoGenerator()
        {
            _random = new Random();
        }

        private static readonly char[][] KeyboardLayoutLower =
        {
            new[] {'`', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '='},
            new[] {'\0', 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\\'},
            new[]    {'\0', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\''},
            new[]     {'\0', 'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/'},
            new[]       {'\0',  '\0',  ' ', ' ', ' ', ' ', ' ', '\0',  '\0'}
        };

        private static readonly char[][] KeyboardLayoutUpper =
        {
            new[] {'~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '+'},
            new[] {'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '{', '}', '|'},
            new[]    {'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ':', '"'},
            new[]       {'Z', 'X', 'C', 'V', 'B', 'N', 'M', '<', '>', '?'},
            new[]          {'\0', '\0', ' ', ' ', ' ', ' ', ' ', '\0', '\0'}
        };

        private static readonly TupleList<int,int> Neighbours = new TupleList<int, int>
        {
            {-1, -1}, {-1, 0}, {-1, +1},
            { 0, -1},          { 0, +1},
            {+1, -1}, {+1, 0}, {+1, +1}
        };

        //Rider can't do the new tuple syntax yet on osx/linux
        //So we have to do it the ugly way :(
        private static readonly ValueTuple<string, string>[]  SuffixErrors =
        {
            new ValueTuple<string, string>("ery", "ary" ),
            new ValueTuple<string, string>("er", "ar" ),
            new ValueTuple<string, string>("asy", "acy"),
            new ValueTuple<string, string>("ely", "ally"),
            new ValueTuple<string, string>("ely", "ly"),
            new ValueTuple<string, string>("ent", "ant")
        };

        private static readonly ValueTuple<string, string>[]  SimilarSoundingLetters =
        {
            new ValueTuple<string, string>("g", "j" ),
            new ValueTuple<string, string>("c", "s" ),
            new ValueTuple<string, string>("c", "z" ),
            new ValueTuple<string, string>("s", "s" ),
            new ValueTuple<string, string>("a", "u"),
            new ValueTuple<string, string>("k", "c"),
        };

        private static readonly ValueTuple<string, string>[]  CommonMisspellings =
        {
            new ValueTuple<string, string>("your", "you're" ),
            new ValueTuple<string, string>("their", "they're" ),
            new ValueTuple<string, string>("their", "there" ),
            new ValueTuple<string, string>("there", "they're" ),
            new ValueTuple<string, string>("it", "it's"),
            new ValueTuple<string, string>("where", "wear"),
            new ValueTuple<string, string>("we're", "were")
        };

        /// <summary>
        /// Attempts to find the given letter in the defined keyboard layouts
        /// Will return null if the letter does not exist in the layouts, otherwise a tuple
        /// consiting of the key's xPos, yPos and the corresponding layout will be returned.
        /// </summary>
        /// <param name="letter">The letter to look for</param>
        /// <returns>A tuple holding the xPos, yPos and layout</returns>
        private static Tuple<int, int, char[][]> LookupLetterOnLayout(char letter)
        {
            foreach (var keyboardLayout in new[] {KeyboardLayoutLower, KeyboardLayoutUpper})
            {
                for (int i = 0; i < keyboardLayout.Length; i++)
                {
                    for (int j = 0; j < keyboardLayout[i].Length; j++)
                    {
                        if (keyboardLayout[i][j] == letter)
                        {
                            //xPos = j;
                            //yPos = i;
                            return new Tuple<int, int, char[][]>(j, i, keyboardLayout);
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Attempts to find the surrounding keys of a given letter on a physical keyboard layout
        /// </summary>
        /// <param name="letter">The origin letter</param>
        /// <returns>An array of letters that surround the origin letter</returns>
        private char[] FindNearestKeys(char letter)
        {
            var keyData = LookupLetterOnLayout(letter);
            if (keyData == null)
            {
                return new[] {letter};
            }

            int xPos = keyData.Item1;
            int yPos = keyData.Item2;
            var keyboardLayout = keyData.Item3;

            var validNeighbours = new List<char>();
            foreach (var neigbhour in Neighbours)
            {
                if (xPos + neigbhour.Item1 >= 0 &&
                    yPos + neigbhour.Item2 >= 0 &&
                    xPos + neigbhour.Item1 < keyboardLayout[yPos + neigbhour.Item2].Length -1 &&
                    yPos + neigbhour.Item2 < keyboardLayout.Length - 1)
                {
                    if (keyboardLayout[yPos + neigbhour.Item2][xPos + neigbhour.Item1] != '\0')
                    {
                        validNeighbours.Add(keyboardLayout[yPos + neigbhour.Item2][xPos + neigbhour.Item1]);
                    }
                }
            }

            return validNeighbours.ToArray();
        }

         //TODO: maybe make this an extension method?
         private string RandomStringReplace(string text, string target, string replacement, int chance = 100)
         {
             if (text.Length <= 0 || text.IndexOf(target, StringComparison.InvariantCultureIgnoreCase) == -1)
             {
                 return text;
             }

             target = Regex.Escape(target);
             var sb = new StringBuilder(text);

             foreach (Match match in Regex.Matches(text, target, RegexOptions.IgnoreCase))
             {
                 if (chance >= _random.Next(1, 101))
                 {
                     //For now, just check if the word is all uppercase and transform the replacement if necessary
                     if (text.ToUpperInvariant() == text)
                     {
                         replacement = replacement.ToUpperInvariant();
                     }

                     //sb.Replace(target, replacement, match.Index, match.Length);
                     sb.Remove(match.Index, match.Length);
                     sb.Insert(match.Index, replacement);
                 }
             }

             return sb.ToString();

         }



        /// <summary>
        /// Takes an array of (string, string) tuples that define the possible Replacements, e.g. ("ary", "ery") and
        /// will replace the corresponding characters. The mapping is bidirectional.
        /// Needs a lambda function to filter the list for typos
        /// </summary>
        /// <param name="compareFunction">Lambda function that will be used to search the list for typos</param>
        /// <param name="dict">The list of replacements as tuples</param>
        /// <param name="word">The word to perform the replacement on</param>
        /// <returns>The new word</returns>
        private string DictionaryReplacer(Func<string, string, bool> compareFunction, ValueTuple<string, string>[] dict,
            string word, int chance = 100)
        {
            var suffixes = dict.Where(x => compareFunction(word, x.Item1) || compareFunction(word, x.Item2)).ToArray();

            if (suffixes.Length > 0)
            {
                var suffix = suffixes[_random.Next(0, suffixes.Length)];

                //Allow bidirectional swaps
                if (word.EndsWith(suffix.Item2))
                {
                    suffix = new ValueTuple<string, string>(suffix.Item2, suffix.Item1);
                }

                return RandomStringReplace(word, suffix.Item1, suffix.Item2, chance);
            }
            else
            {
                return word;
            }
        }

        public string SuffixTypo(string word, int chance)
        {
            Func<string, string, bool> endComparator =
                (string x, string y) => x.EndsWith(y, StringComparison.InvariantCultureIgnoreCase);
            return DictionaryReplacer(endComparator, SuffixErrors, word, chance);
        }

        /// <summary>
        /// Replaces letters in the word with others that sound similar,
        /// For example: quick -> quicc, Dog -> Doj
        /// </summary>
        /// <param name="word">The word to apply the typo to</param>
        /// <param name="chance">The chance for a letter to be replaced (0-100)</param>
        /// <returns>The misstyped word</returns>
        public string SimilarSoundTypo(string word, int chance)
        {
            Func<string, string, bool> letterMatch =
                (string x, string y) => x.IndexOf(y, StringComparison.Ordinal) != -1;
            return DictionaryReplacer(letterMatch, SimilarSoundingLetters, word, chance);
        }

        public string CommonMispelling(string word, int chance)
        {
            Func<string, string, bool> wordMatch =
                (string x, string y) => x.IndexOf(y, StringComparison.Ordinal) != -1;
            return DictionaryReplacer(wordMatch, CommonMisspellings, word, chance);
        }

        /// <summary>
        /// Randomly flips the case of the word
        /// For example: dog -> Dog
        /// </summary>
        /// <param name="word"></param>
        /// <param name="chance"></param>
        /// <returns></returns>
        public string RandomCapsWord(string word, int chance)
        {
            if (chance >= _random.Next(1, 101))
            {
                var sb = new StringBuilder(word);
                sb[0] = char.IsUpper(word[0]) ? char.ToLowerInvariant(sb[0]) : char.ToUpperInvariant(sb[0]);
                return sb.ToString();
            }
            else
            {
                return word;
            }
        }


        /// <summary>
        /// Randomly doubles letters in the word
        /// </summary>
        /// <param name="word">The word to double some letters in</param>
        /// <param name="chance">The chance for a letter to be doubled</param>
        /// <returns>The misstyped worrd</returns>
        public string DoubleLetters(string word, int chance)
        {
            var sb = new StringBuilder(word);

            for (int i = 0; i < sb.Length; i++)
            {
                if (chance >= _random.Next(1, 101))
                {
                    sb.Insert(i+1, char.ToLowerInvariant(sb[i]));
                    i++;
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// Makes the shift key sticky, causing capitalisation of letters to "leak" into the next letters.
        /// Computer -> COmputer
        /// </summary>
        /// <param name="word">The word to apply the typo on</param>
        /// <param name="chance">Chance of having the shift key stick</param>
        /// <returns>THe mistyped word</returns>
        public string ShiftLeak(string word, int chance)
        {
            if (word.Length <= 1)
            {
                return word;
            }

            var sb = new StringBuilder(word);
            for (int i = 0; i < sb.Length; i++)
            {
                if (char.IsUpper(sb[i]) && chance >= _random.Next(1, 101))
                {
                    sb[i+1] = char.ToUpper(sb[i+1]);
                    i++;
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// Removes one letter in doubleletter pairs
        /// door -> dor
        /// </summary>
        /// <param name="word">The word to apply the typo on</param>
        /// <param name="chance">The chance for a doubled letter to be removed</param>
        /// <returns>The mistyped word</returns>
        public string CollapseDoubleLetters(string word, int chance)
        {
            if (word.Length <= 1)
            {
                return word;
            }

            char lastLetter = '\0';
            var doubleLetters = new List<int>();

            for (int i = 0; i < word.Length; i++)
            {
                if (lastLetter == word[i])
                {
                    doubleLetters.Add(i);
                }
                lastLetter = word[i];
            }

            StringBuilder sb = new StringBuilder(word);

            foreach (var position in doubleLetters)
            {
                if (chance >= _random.Next(1, 101))
                {
                    sb.Remove(position, 1);
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// Randomly swaps letters with their neighbours on the keyboard.
        /// the -> th3
        /// </summary>
        /// <param name="word">The word to mistype</param>
        /// <param name="chance">Percent chance from 0-100 for the mistype to happen</param>
        /// <returns>The mistyped word</returns>
        public string MistypeKeys(string word, int chance)
        {
            if (word.Length <= 1)
            {
                return word;
            }

            StringBuilder sb = new StringBuilder(word);

            for (int i = 0; i < sb.Length; i++)
            {
                if (chance >= _random.Next(1, 101))
                {
                    var neighbours = FindNearestKeys(sb[i]);
                    sb[i] = neighbours[_random.Next(0, neighbours.Length)];
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// Randomly swaps two adjacent letters in the word
        /// the -> teh or hte
        /// </summary>
        /// <param name="word">The word to perform the typo on</param>
        /// <returns>The transposed word</returns>
        public string TransposeLetters(string word)
        {
            if (word.Length <= 1)
            {
                return word;
            }

            StringBuilder sb = new StringBuilder(word);
            int letterPosition = _random.Next(0, word.Length - 1);
            char temp = sb[letterPosition];
            sb[letterPosition] = sb[letterPosition + 1];
            sb[letterPosition + 1] = temp;
            return sb.ToString();
        }

    }

    public class TupleList<T1, T2> : List<Tuple<T1, T2>>
    {
        public void Add( T1 item, T2 item2 )
        {
            Add( new Tuple<T1, T2>( item, item2 ) );
        }
    }
}